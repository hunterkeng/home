## Env variable
https://create-react-app.dev/docs/adding-custom-environment-variables/

## All
1. Do Not forget to run `npm run build` when commit
2. account dshh->264***@qq.com ->index project
3. account h***k->max***@hotmail.com ->home project
## Check build to confirm sync or not


## Publish to https://h***k***.gitlab.io/home/ multiple remote master
1. git remote add origin git@gitlab.com-hunter:kiwi**/home.git
2. git remote set-url --add origin git@gitlab.com-max:hunterke**/home.git
3. git remote -v to verify
4. generate another ssh key, add it to another gitlab account
5. configure below in the ssh/config file
#hunter account
Host gitlab.com-hunter
	HostName gitlab.com
	User git
	IdentityFile ~/.ssh/id_rsa

#max account
Host gitlab.com-max
	HostName gitlab.com
	User git
	IdentityFile ~/.ssh/id_rsa2

## Verify data
1. Add latest data to sheet 1 of nzcovid.xlsx(only new data)
2. java -cp target/COVID-1.0-SNAPSHOT.jar nzcase.ExcelParser


This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

## Automatic update
This project has been mirrored by [dshh](https://gitlab.com/h***k***/home)


## API
1. API uses google cloud funtions. Read data from firestore and process by cloud functions then return to the users

## Craw note
1. Craw project use gitlab schedule feature. And run node craw.js at a certain time.
2. Craw Ministry of Health website data at 1:30pm, 2:30pm, 3:30pm and 4:30pm everyday