import React from "react";
import axios from "axios";

import { translate } from "../js/translate";
// import logo from "../img/icon.svg"

class News extends React.Component {

    constructor(props) {
        super(props);

        this.newsData = [];
        this.state = { nzNews: this.nzNews, selectedNews: {}, showEditDiv: false, newsData: [] };

    }

    componentDidMount() {
        window.addEventListener('scroll', this.props.hideMenu);
        var db = this.props.fb.firestore();
        db.collection("news").get().then((querySnapshot) => {
            querySnapshot.forEach((doc) => {
                console.log(`${doc.id} => ${JSON.stringify(doc.data())}`);
                this.newsData.push(doc.data());
            })
            this.newsData = this.newsData.sort((x, y) => x.id > y.id ? -1 : 1);
            this.setState({ newsData: this.newsData })
        }).catch(function (error) {
            console.error("Error getting document: ", error);
        });



        axios.get(`http://us-central1-test-3a7af.cloudfunctions.net/imageUpdate`)
            .then((response) => {
                console.log(`--- ${JSON.stringify(response)}---- `)
                console.log(`--- us central1---- `)
                let data = response.data;
                let lastPiece = data[data.length - 1];
                this.setState({ selectedNews: lastPiece })

                // this.setState(preState => ({ eventsArr: eventsArr, eventUS: eventsArrUS.length > 0 ? eventsArrUS[0] : this.eventUS, eventCN: eventsArrCN.length > 0 ? eventsArrCN[0] : this.eventCN, event: preState.event }));
            })
            .catch(function (err) {
                console.log(err)
                console.log(JSON.stringify(err))
                // console.error("Getting file error..." + JSON.stringify(err));
                // this.setState(preState => ({ eventsArr: [], eventUS: this.eventUS, eventCN: this.eventCN, event: preState.event }));

            })



    }

    selectNews(selectedNews) {
        console.log(`--------news selectNews: ${JSON.stringify(selectedNews)}----------`)
        this.setState({ selectedNews: selectedNews })
    }

    handleChange(e) {
        console.log(`--------news handleChange: ${e}--------------`)
        try {

            if (!this.state.selectedNews.id) {
                let d = new Date();
                let dt = d.toDateString().substr(4);
                let id = d.toISOString().replace(/:/g, "").replace(/\./g, "");

                let temp = this.state.selectedNews;
                temp.id = id;
                temp.date = dt;

                this.setState({
                    selectedNews: temp
                })

            }
            const target = e.target;
            const value = target.value;
            const name = target.name;

            this.setState(prevState => {
                let temp = prevState.selectedNews;
                temp[name] = value;
                return temp;
            });

        } catch (err) {
            console.error(`--------manage handleChange: ${JSON.stringify(err)}--------------`)
        }
    }

    update(e) {
        console.log(`--------manage update: ${e}--------------`)
        this.props.update(this.state.selectedNews)
    }

    del(e) {
        console.warn(`--------manage del: ${e}--------------`)
        this.props.del(e)
    }

    show() {
        this.setState({ showEditDiv: true })
    }

    render() {

        return (

            <div className="mt-5" onClick={this.props.hideMenu}>
                <br />
                <div className="container text-center text-capitalize">
                    <h3>{translate("newsHeader")}</h3>
                </div>
                <br />
                <div className="container marketing">
                    <div className="row">
                        {this.state.selectedNews.urls && this.state.selectedNews.urls.map((ns, index) => {
                            return <div className="col-lg-12" key={index} onClick={() => this.selectNews(ns)}>
                                <h2>{ns.desc}</h2>
                                <img alt="" className="bd-placeholder-img" width="100%" height="780" src={ns.url}></img>
                            </div>

                        })}
                    </div>
                </div>

                <br></br>
                <br></br>

                {this.state.showEditDiv && <div><h3>If you are admin, click one row to update</h3>
                    <br></br>
                    <br></br>
                    <div className="form-row">
                        <input type="text" name="date" className="form-control col-2" value={this.state.selectedNews.date} placeholder="Date" onChange={(e) => this.handleChange(e)}></input>
                        <textarea type="text" name="summary" className="form-control col-2" value={this.state.selectedNews.summary} placeholder="Summary" onChange={(e) => this.handleChange(e)}></textarea>
                        <input type="text" name="source" className="form-control col-2" value={this.state.selectedNews.source} placeholder="Source" onChange={(e) => this.handleChange(e)}></input>
                        <input type="text" name="link" className="form-control col-2" value={this.state.selectedNews.link} placeholder="Link" onChange={(e) => this.handleChange(e)}></input>
                        <button className="btn btn-secondary col-2" onClick={(e) => this.update(e)}>Add/Update</button>
                    </div>
                </div>}

            </div>
        );
    }

}

export default News;