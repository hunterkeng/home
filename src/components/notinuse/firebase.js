import React from "react";

// Firebase App (the core Firebase SDK) is always required and
// must be listed before other Firebase SDKs
import * as firebase from "firebase/app";

// Add the Firebase services that you want to use
import "firebase/auth";
import "firebase/firestore";

var firebaseConfig = {
  apiKey: "AIzaSyDR8762X1sww0n9yyoKklutUAV7qsOAK-U",
  authDomain: "test-3a7af.firebaseapp.com",
  databaseURL: "https://test-3a7af.firebaseio.com",
  projectId: "test-3a7af",
  storageBucket: "test-3a7af.appspot.com",
  messagingSenderId: "801396039695",
  appId: "1:801396039695:web:b689f124758e2d73"
};

firebase.initializeApp(firebaseConfig);

class FirebaseTest extends React.Component {

  constructor() {
    super()
    this.state = { user: {} }
  }

  handleClick() {
    console.log("-------handleClick--------")
    firebase.auth().signInAnonymously().catch(function (error) {
      // Handle Errors here.
      let errorCode = error.code;
      let errorMessage = error.message;

      console.error(`-------handleClick ${errorCode}-${errorMessage}-------`)
    });


    firebase.auth().onAuthStateChanged(function (user) {
      if (user) {
        // User is signed in.
        var isAnonymous = user.isAnonymous;
        var uid = user.uid;
        console.log(`-------handleClick ${uid} isAnonymous: ${isAnonymous}--------`)
      } else {
        // User is signed out.
      }
    });
  }

  getData() {
    var db = firebase.firestore();
    db.collection('cases').get()
      .then((snapshot) => {
        snapshot.forEach((doc) => {
          console.log(doc.id, '=>', doc.data());
        });
      })
      .catch((err) => {
        console.log('Error getting documents', err);
      });
  }

  render() {
    return (
      <div>
        <button onClick={(e) => this.handleClick(e)}>Signin</button>
        <button onClick={(e) => this.getData(e)}>GetData</button>

      </div>
    );
  }

}

export default FirebaseTest;