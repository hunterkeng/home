import React from "react";

import { translate } from "../../js/translate";

class Projects extends React.Component {

    constructor(props) {
        super(props);

        this.projectsUS = { "id": "", "date": "5 March 2020", "summary": "Church Event", "source": "rnz", "link": "/Church" };

        this.state = { selectedProject: {}, showEditDiv: false };

    }

    selectProjects(selectedProject) {
        console.log(`--------projects selectProjects: ${JSON.stringify(selectedProject)}----------`)
        this.setState({ selectedProject: selectedProject })
    }

    handleChange(e) {
        console.log(`--------projects handleChange: ${e}--------------`)
        try {

            if (!this.state.selectedProject.id) {
                let d = new Date();
                let dt = d.toDateString().substr(4);
                let id = d.toISOString().replace(/:/g, "").replace(/\./g, "");

                let temp = this.state.selectedProject;
                temp.id = id;
                temp.date = dt;

                this.setState({
                    selectedProject: temp
                })

            }
            const target = e.target;
            const value = target.value;
            const name = target.name;

            this.setState(prevState => {
                let temp = prevState.selectedProject;
                temp[name] = value;
                return temp;
            });

        } catch (err) {
            console.error(`--------projects handleChange: ${JSON.stringify(err)}--------------`)
        }
    }

    update(e) {
        console.log(`--------projects update: ${e}--------------`)
        this.props.update(this.state.selectedProject)
    }

    del(e) {
        console.warn(`--------projects del: ${e}--------------`)
        this.props.del(e)
    }

    show() {
        this.setState({ showEditDiv: true })
    }

    render() {

        return (

            <div>
                <br />
                <div className="container text-center text-capitalize">
        <h3 style={{display: 'inline'}}>{translate("projectsOverview")}</h3>
                </div>
                <br />
                <div className="px-3 table-responsive">
                    <div className="row">
                        <table className="table table-striped">
                            <thead>
                                <tr>
                                    <th className="border-top-0 pt-0 w-10 text-capitalize">{translate("date")}</th>
                                    <th className="border-top-0 pt-0 w-80 text-capitalize">{translate("summary")}</th>
                                    <th className="border-top-0 pt-0 w-10 text-capitalize text-primary" onDoubleClick={(e) => this.show(e)}>{translate("source")}</th>
                                    {this.state.showEditDiv && <th className="border-top-0 pt-0 w-10 text-capitalize">{translate("operation")}</th>}
                                </tr>
                            </thead>
                            <tbody>
                                {this.props.projects && this.props.projects.map((project, index) => {
                                    return <tr key={index} onClick={() => this.selectProjects(project)}>
                                        <td>{project.date}</td>
                                        <td className="w-50">{project.summary}</td>
                                        <td><a href={project.link}>{project.source}</a> </td>
                                        {this.state.showEditDiv && <td value={project} onClick={() => this.del(project)}><button className="btn btn-danger" >{translate("delete")}</button></td>}
                                    </tr>
                                })}
                            </tbody>
                        </table>
                    </div>
                </div>

                <br></br>
                <button type="Signin" className="btn btn-primary" onClick={(e) => this.props.adminSignin(e)}>{translate("signin")}</button>
                <br></br>
                <br></br>

                {this.state.showEditDiv && <div><h3>If you are admin, click one row to update</h3>
                    <br></br>
                    <br></br>
                    <div className="form-row">

                        <input type="text" name="date" className="form-control col-2" placeholder={translate("date")} value={this.state.selectedProject.date} onChange={(e) => this.handleChange(e)}></input>
                        <textarea type="text" name="summary" className="form-control col-2" placeholder={translate("summary")} value={this.state.selectedProject.summary} onChange={(e) => this.handleChange(e)}></textarea>
                        <input type="text" name="source" className="form-control col-2" placeholder={translate("source")} value={this.state.selectedProject.source} onChange={(e) => this.handleChange(e)}></input>
                        <input type="text" name="link" className="form-control col-2" placeholder={translate("link")} value={this.state.selectedProject.link} onChange={(e) => this.handleChange(e)}></input>
                        <select className="form-control col-2" name="lang" value={this.state.selectedProject.lang} onChange={(e) => this.handleChange(e)}>
                            <option value="en-US" >English</option>
                            <option value="zh-CN">中文</option>
                        </select>
                        <button className="btn btn-secondary col-2" onClick={(e) => this.update(e)}>Add/Update</button>
                    </div>
                </div>}

            </div>
        );
    }

}

export default Projects;