import React from "react";

import { translate } from "../../js/translate";

class Comments extends React.Component {

    constructor(props) {
        super(props);

        this.state = { comment: { "id": "", "date": "5 March 2020", "uid": "anonymous1", "content": "Hello World!" } }

    }

    handleChange(e) {
        console.log(`--------manage handleChange: ${e}--------------`)
        try {

            let temp = this.state.comment;
            temp.content = e.target.value;

            this.setState({
                comment: temp
            })

        } catch (err) {
            console.error(`--------manage handleChange: ${JSON.stringify(err)}--------------`)
        }

    }

    update(e) {
        console.log(`--------comments update: ${e}--------------`)
        let d = new Date();
        let dt = d.toDateString().substr(4) + " " + d.toTimeString().split(' ')[0];

        let id = d.toISOString().replace(/:/g, "").replace(/\./g, "");

        let temp = this.state.comment;
        temp.id = id;
        temp.date = dt;
        temp.uid = "Anonymous_User_" + this.props.user.uid.substr(0, 5);
        this.props.update(temp);
    }

    del(e) {
        console.log(`--------comments delete: ${e}--------------`)
        this.props.del(this.state.comment)
    }

    render() {

        return (

            <div>
                <br />
                <div className="container text-center text-capitalize">
                    <h3>{translate("leaveComment")}</h3>
                </div>
                <br />
                <div className="px-3 table-responsive">
                    <div className="row">
                        <table className="table table-striped">
                            <thead>
                                <tr>
                                    <th className="border-top-0 pt-0 w-10 text-capitalize">{translate("date")}</th>
                                    <th className="border-top-0 pt-0 w-20 text-capitalize">{translate("user")}</th>
                                    <th className="border-top-0 pt-0 w-70 text-capitalize">{translate("content")}</th>
                                </tr>
                            </thead>
                            <tbody>
                                {this.props.comments && this.props.comments.map((comment, index) => {
                                    return <tr key={index}>
                                        <td>{comment.date}</td>
                                        <td>{comment.uid}</td>
                                        <td>{comment.content}</td>
                                    </tr>
                                })}

                                <tr>
                                    <td colSpan={2}><textarea type="text" name="comment" className="form-control" value={this.state.comment.content} onChange={(e) => this.handleChange(e)}></textarea></td>
                                    <td><button className="btn btn-secondary" onClick={(e) => this.update(e)}>{translate("submit")}</button></td>
                                </tr>

                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        );
    }

}

export default Comments;