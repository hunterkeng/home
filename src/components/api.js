import React from "react";

import { translate } from "../js/translate";

class API extends React.Component {

    constructor(props) {
        super(props);

        this.apis = [];

        this.state = { selectedAPI: {}, showEditDiv: false, apis: [] };

    }

    componentDidMount() {
        window.addEventListener('scroll', this.props.hideMenu);
        var db = this.props.fb.firestore();
        db.collection("apis").get().then((querySnapshot) => {
            querySnapshot.forEach((doc) => {
                console.log(`${doc.id} => ${JSON.stringify(doc.data())}`);
                this.apis.push(doc.data());
            })

            this.setState({ apis: this.apis })
        }).catch(function (error) {
            console.error("Error getting document: ", error);
        });
    }

    selectAPI(selectedAPI) {
        console.log(`--------api selectedAPI: ${JSON.stringify(selectedAPI)}----------`)
        this.setState({ selectedAPI: selectedAPI })
    }

    handleChange(e) {
        console.log(`--------api handleChange: ${e}--------------`)
        try {

            if (!this.state.selectedAPI.date) {
                let d = new Date();
                let dt = d.toDateString().substr(4);

                let temp = this.state.selectedAPI;
                temp.date = dt;

                this.setState({
                    selectedAPI: temp
                })

            }
            const target = e.target;
            const value = target.value;
            const name = target.name;

            this.setState(prevState => {
                let temp = prevState.selectedAPI;
                temp[name] = value;
                return temp;
            });

        } catch (err) {
            console.error(`--------api handleChange: ${JSON.stringify(err)}--------------`)
        }
    }

    update(e) {
        console.log(`--------api update: ${e}--------------`)
        this.props.update(this.state.selectedAPI)
    }

    del(e) {
        console.warn(`--------api del: ${e}--------------`)
        this.props.del(e)
    }

    show() {
        this.setState({ showEditDiv: true })
    }

    render() {

        return (

            <div className="mt-5" onClick={this.props.hideMenu}>
                <br />
                <div className="container text-center text-capitalize">
                    <h3 style={{ display: 'inline' }}>{translate("api")}</h3>
                </div>
                <br />
                <div className="px-3 table-responsive">
                    <div className="row">
                        <table className="table table-striped">
                            <thead>
                                <tr>
                                    <th className="border-top-0 pt-0 w-10 text-capitalize">{translate("date")}</th>
                                    <th className="border-top-0 pt-0 w-80 text-capitalize">{translate("summary")}</th>
                                    <th className="border-top-0 pt-0 w-10 text-capitalize text-primary" onDoubleClick={(e) => this.show(e)}>{translate("source")}</th>
                                    {this.state.showEditDiv && <th className="border-top-0 pt-0 w-10 text-capitalize">{translate("operation")}</th>}
                                </tr>
                            </thead>
                            <tbody>
                                {this.state.apis && this.state.apis.map((api, index) => {
                                    return <tr key={index} onClick={() => this.selectAPI(api)}>
                                        <td>{api.date}</td>
                                        <td className="w-50">{api.summary}</td>
                                        <td><a href={api.link}>{api.source}</a> </td>
                                        {this.state.showEditDiv && <td value={api} onClick={() => this.del(api)}><button className="btn btn-danger" >{translate("delete")}</button></td>}
                                    </tr>
                                })}
                            </tbody>
                        </table>
                    </div>
                </div>

                <br></br>
                <br></br>

                {this.state.showEditDiv && <div><h3>If you are admin, click one row to update</h3>
                    <br></br>
                    <br></br>
                    <div className="form-row">

                        <input type="text" name="id" className="form-control col-2" placeholder={translate("id")} value={this.state.selectedAPI.id} onChange={(e) => this.handleChange(e)}></input>
                        <input type="text" name="date" className="form-control col-2" placeholder={translate("date")} value={this.state.selectedAPI.date} onChange={(e) => this.handleChange(e)}></input>
                        <textarea type="text" name="summary" className="form-control col-2" placeholder={translate("summary")} value={this.state.selectedAPI.summary} onChange={(e) => this.handleChange(e)}></textarea>
                        <input type="text" name="source" className="form-control col-2" placeholder={translate("source")} value={this.state.selectedAPI.source} onChange={(e) => this.handleChange(e)}></input>
                        <input type="text" name="link" className="form-control col-2" placeholder={translate("link")} value={this.state.selectedAPI.link} onChange={(e) => this.handleChange(e)}></input>
                        <button className="btn btn-secondary col-2" onClick={(e) => this.update(e)}>Add/Update</button>
                    </div>
                </div>}

            </div>
        );
    }

}

export default API;