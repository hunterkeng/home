import React from "react";

import ReactEcharts from 'echarts-for-react';
import echarts from 'echarts';

import { translate } from "../js/translate";
import nzMap from '../map/nz.geo.json';
import nzMapCN from '../map/nz.geo.cn.json';

import { } from 'dotenv/config'

import firebase from "firebase/app";

import "firebase/firestore";

const firebaseConfig = {
    apiKey: "AIzaSyDR8762X1sww0n9yyoKklutUAV7qsOAK-U",
    authDomain: "localhost:3000",
    databaseURL: "https://test-3a7af.firebaseio.com",
    projectId: "test-3a7af",
    storageBucket: "test-3a7af.appspot.com",
    messagingSenderId: "801396039695",
    appId: "1:801396039695:web:b689f124758e2d73"
  };

firebase.initializeApp(firebaseConfig);

var db = firebase.firestore();

class Overview extends React.Component {

    constructor(props) {
        super(props);

        this.selectedRegion = { id: "newzealand", date: "", numsNew: 0, numsAll: 0, numsCured: 0, numsDeath: 0 }

        this.defaultTableData = { id: "auckland", date: "28 Feb 2020", numsNew: 0, numsAll: 1, numsCured: 0, numsDeath: 0 };
        this.latest = { id: '02-28', numsNew: 0, numsAll: 1, numsCured: 0, numsDeath: 0 };

        this.tableData = [];
        this.lastWeekData = [];
        this.state = { selectedRegion: this.selectedRegion, showEditDiv: false, desc: true, tableData: [this.defaultTableData], lastWeekData: [this.latest] };

        this.mapGetOption = this.mapGetOption.bind(this);
        this.lineGetOption = this.lineGetOption.bind(this);

        this.largest = 500;

        this.data = []

    }

    componentDidMount() {
        window.addEventListener('scroll', this.props.hideMenu);
        // var db = this.props.fb.firestore();
        // db.collection("cases").get().then((querySnapshot) => {
        //     querySnapshot.forEach((doc) => {
        //         console.log(`${doc.id} => ${JSON.stringify(doc.data())}`);
        //         this.tableData.push(doc.data());
        //     })

        //     this.tableData = this.tableData.sort((x, y) => parseInt(x.numsAll) > parseInt(y.numsAll) ? -1 : 1);
        //     this.setState({ tableData: this.tableData })
        //     this.largest = this.tableData[0].numsAll;
        // }).catch(function (error) {
        //     console.error("Error getting document: ", error);
        // });

        // axios.get(`https://us-central1-test-3a7af.cloudfunctions.net/cases`)

        db.collection("cases").get().then((querySnapshot) => {
            querySnapshot.forEach((doc) => {
                this.data.push(doc.data())
            });

            this.tableData = this.data.sort((x, y) => parseInt(x.numsAll) > parseInt(y.numsAll) ? -1 : 1);
            this.setState({ tableData: this.tableData })
            this.largest = this.tableData[0].numsAll;
        });

        
        // axios.get(`${process.env.REACT_APP_API_URL}/cases`)
        //     .then((response) => {
                

            // })
            // .catch(function (err) {
            //     console.error("Error getting cases document: ", err);

            // })


        // axios.get(`https://us-central1-test-3a7af.cloudfunctions.net/dayUpdate2021`)
        // axios.get(`${process.env.REACT_APP_API_URL}/dayUpdate2021`)
        //     .then((response) => {
        //         console.log(`--- us central1 more---- `)
        //         let data = response.data;

        //         this.latest = data[data.length - 1];
        //         let officialUpdateDate = this.latest.officialUpdateDate;
        //         officialUpdateDate = officialUpdateDate.replace('Last updated', '');
        //         officialUpdateDate = officialUpdateDate.replace(',', '');
        //         officialUpdateDate = officialUpdateDate.replace('.', '');

        //         this.latest.officialUpdateDate = officialUpdateDate;
        //         this.latest.orgUpdateDate = officialUpdateDate;

        //         this.setState({ lastWeekData: data })
        //     })
        //     .catch(function (err) {
        //         console.error("Error getting more document: ", err);

        //     })

        db.collection("dayUpdate2021").get().then((querySnapshot) => {
            querySnapshot.forEach((doc) => {
                console.log(`${doc.id} => ${JSON.stringify(doc.data())}`);
                this.lastWeekData.push(doc.data());
            })
            this.latest = this.lastWeekData[this.lastWeekData.length - 1];
            this.setState({ lastWeekData: this.lastWeekData })
        }).catch(function (error) {
            console.error("Error getting document: ", error);
            this.setState({ lastWeekData: this.lastWeekData })
        });
    }

    selectRegion(selectedRegion) {
        console.log(`--------overview selectRegion: ${JSON.stringify(selectedRegion)}--------------`)
        this.setState({ selectedRegion: selectedRegion })
    }

    handleChange(e) {
        console.log(`--------overview handleChange: ${e}--------------`)
        try {
            if (!this.state.selectedRegion.date) {
                let d = new Date();
                let dt = d.toISOString().substr(0, 10);// 03-25

                let temp = this.state.selectedRegion;
                temp.date = dt;

                this.setState({
                    selectedRegion: temp
                })

            }
            const target = e.target;
            const value = target.value;
            const name = target.name;

            this.setState(prevState => {
                let temp = prevState.selectedRegion;
                temp[name] = isNaN(value) ? value : parseInt(value);
                return temp;
            });

        } catch (err) {
            console.error(`--------overview handleChange: ${JSON.stringify(err)}--------------`)
        }

    }

    update(e) {
        console.log(`--------overview update: ${JSON.stringify(e)}--------------`)
        if (e.id === "") {
            alert("Please select region");
        } else {
            this.props.update(this.state.selectedRegion)
        }
    }

    sort(field, desc, e) {
        console.log(`--------App sort:--------------`)
        this.setState(preState => {
            preState.desc = !preState.desc;
            let temp = preState.tableData;
            temp = temp.sort((x, y) => parseInt(x[`${field}`]) > parseInt(y[`${field}`]) ? -1 : 1);
            if (desc) {
                temp.reverse();
            }
            preState.tableData = temp;

            return preState;
        })

    }

    show() {
        this.setState({ showEditDiv: true })
    }

    mapGetOption() {
        console.log("---------mapGetOption---------")
        let numsAll = this.largest;
        let hundreds = Math.pow(10, (numsAll + "").length - 1); //451 - > 100
        let mapMax = Math.ceil(parseInt(numsAll) / hundreds) * hundreds;

        let mapData = [];
        this.state.tableData.map(rowData =>
            mapData.push({ name: translate(rowData.id), value: isNaN(rowData.numsAll) ? 0 : parseInt(rowData.numsAll) })
        );

        let option = {
            title: {
                text: translate("homeHeader"),
                subtext: translate("dataSource"),
                sublink: 'https://www.health.govt.nz/our-work/diseases-and-conditions/covid-19-novel-coronavirus/covid-19-current-cases',
                left: 'left'
            },
            tooltip: {
                trigger: 'item',
                showDelay: 0,
                transitionDuration: 0.2,
                formatter: function (params) {
                    var value = (params.value + '').split('.');
                    value = value[0].replace(/(\d{1,3})(?=(?:\d{3})+(?!\d))/g, '$1,');
                    return params.seriesName + '<br/>' + params.name + ': ' + value;
                }
            },

            visualMap: {
                left: 'right',
                min: 0,
                max: mapMax,
                splitNumber: 5,
                color: ['#d94e5d', '#eac736', '#50a3ba'],
                textStyle: {
                    color: '#000'
                }
            },
            series: [
                {
                    name: translate("cases"),
                    type: 'map',
                    roam: false,
                    map: 'NZ',
                    emphasis: {
                        label: {
                            show: true
                        }
                    },
                    data: mapData
                }
            ]
        };

        return option;
    }

    lineGetOption() {
        let xData = [];
        let yData = [];

        this.state.lastWeekData.map(each => {
            xData.push(each.id);
            yData.push(parseInt(each.numsAll));
            return 0;
        }
        )
        let option = {
            title: {
                text: translate("updateDate")
            },
            xAxis: {
                type: 'category',
                data: xData
            },
            yAxis: {
                type: 'value'
            },
            series: [{
                data: yData,
                type: 'line'
            }]
        };

        return option;
    }

    render() {
        this.props.lang === 'en-US' ? echarts.registerMap("NZ", nzMap) : echarts.registerMap("NZ", nzMapCN);
        let officialUpdateDate = this.latest.officialUpdateDate;
        if ( officialUpdateDate && this.props.lang !== 'en-US') {
            officialUpdateDate = officialUpdateDate.replace(/[^A-Za-z0-9:]/gmi, " ");
            officialUpdateDate = officialUpdateDate.split(" ").reverse();
            officialUpdateDate[1] = translate(officialUpdateDate[1]);
            officialUpdateDate.splice(1, 0, "年");
            officialUpdateDate.splice(4, 0, "日");
            officialUpdateDate = officialUpdateDate.join(" ");
            officialUpdateDate = officialUpdateDate.replace("am", "上午");
            officialUpdateDate = officialUpdateDate.replace("pm", "下午");
            this.latest.officialUpdateDate = officialUpdateDate;
        } else {
            this.latest.officialUpdateDate = this.latest.orgUpdateDate;
        }
        
        return (
            <div className="mt-5" onClick={this.props.hideMenu}>
                <br />
                <div className="container text-center">
                    <h3>{translate("homeHeader")}</h3>
                    <h6>{translate("officialUpdate")} {this.latest.officialUpdateDate}</h6>
                    <h6>{translate("updateTime")} <a href="https://www.health.govt.nz/our-work/diseases-and-conditions/covid-19-novel-coronavirus/covid-19-current-cases">{translate("dataSource")}</a></h6>
                </div>
                <br></br>

                <div className="card-deck mb-3 text-center">
                    <div className="card mb-3 shadow-sm">
                        <div className="card-header">
                            <h4 className="my-0 font-weight-normal">{translate("newCases")}</h4>
                        </div>
                        <div className="card-body">
                            <h1 className="card-title pricing-card-title">{this.latest.numsNew}</h1>
                            <ul className="list-unstyled mt-3 mb-4">
                                <li>&nbsp;</li>
                                <li>{translate("newConfirmed")}: +{this.latest.newConfirmedCases}</li>
                                <li>&nbsp;</li>
                                <li>{translate("newProbable")}: +{this.latest.newProbableCases}</li>
                            </ul>
                        </div>
                    </div>
                    <div className="card mb-3 shadow-sm">
                        <div className="card-header">
                            <h4 className="my-0 font-weight-normal">{translate("totalCases")}</h4>
                        </div>
                        <div className="card-body">
                            <h1 className="card-title pricing-card-title">{this.latest.numsAll}</h1>
                            <ul className="list-unstyled mt-3 mb-4">
                                <li>&nbsp;</li>
                                <li>{translate("totalConfirmed")}: {this.latest.totalConfirmedCases}</li>
                                <li>&nbsp;</li>
                                <li>{translate("totalProbable")}: {this.latest.totalProbableCases}</li>
                            </ul>
                        </div>
                    </div>
                    <div className="card mb-3 shadow-sm">
                        <div className="card-header">
                            <h4 className="my-0 font-weight-normal">{translate("recoveredCases")}</h4>
                        </div>
                        <div className="card-body">
                            <h1 className="card-title pricing-card-title">{this.latest.numsCured}</h1>
                            <ul className="list-unstyled mt-3 mb-4">
                                <li>&nbsp;</li>
                                <li>{translate("newRecovered")}: {this.latest.newRecoveredCases}</li>
                                <li>&nbsp;</li>
                                <li>{translate("inHospital")}: {this.latest.inhospitalCases}</li>
                            </ul>
                        </div>
                    </div>

                    <div className="card mb-3 shadow-sm">
                        <div className="card-header">
                            <h4 className="my-0 font-weight-normal">{translate("death")}</h4>
                        </div>
                        <div className="card-body">
                            <h1 className="card-title pricing-card-title">{this.latest.numsDeath}</h1>
                        </div>
                    </div>
                </div>
                <br />

                <ReactEcharts
                    option={this.mapGetOption()}
                    style={{ height: '600px', width: '100%' }}
                />

                <br></br>
                <ReactEcharts
                    option={this.lineGetOption()}
                    style={{ height: '300px', width: '100%' }}
                />

                <br></br>
                <br></br>


                {this.state.showEditDiv && <div> <h3>If you are admin, add or click one row to update</h3>
                    <br></br>
                    <div className="form-row">
                        <input type="text" name="id" placeholder="Region" className="form-control col-2" defaultValue="newzealand" value={this.state.selectedRegion.id} onChange={(e) => this.handleChange(e)}></input>
                        <input type="text" name="date" placeholder="Date" className="form-control col-2" value={this.state.selectedRegion.date} onChange={(e) => this.handleChange(e)}></input>
                        <input type="number" name="numsNew" placeholder="New" className="form-control col-1" value={this.state.selectedRegion.numsNew} onChange={(e) => this.handleChange(e)}></input>
                        <input type="number" name="numsAll" placeholder="All" className="form-control col-1" value={this.state.selectedRegion.numsAll} onChange={(e) => this.handleChange(e)}></input>
                        <input type="number" name="numsCured" placeholder="Cured" className="form-control col-1" value={this.state.selectedRegion.numsCured} onChange={(e) => this.handleChange(e)}></input>
                        <input type="number" name="numsDeath" placeholder="Death" className="form-control col-1" value={this.state.selectedRegion.numsDeath} onChange={(e) => this.handleChange(e)}></input>
                        <button className="btn btn-secondary  col-2" onClick={(e) => this.update(this.state.selectedRegion)}>Add/Update</button>
                    </div></div>

                }

                <br></br>
                <br></br>

                <div className="px-3 table-responsive">
                    <div className="row">
                        <table className="table table-striped">
                            <thead>
                                <tr>
                                    <th className="border-top-0 pt-0 text-capitalize align-middle">{translate("region")}</th>
                                    <th className="border-top-0 pt-0 text-capitalize align-middle" onClick={(e) => this.sort("numsAll", this.state.desc, e)}><div>{translate("allCases")}{this.state.desc ? <span>  &#8595;</span> : <span>  &#8593;</span>} </div></th>
                                    <th className="border-top-0 pt-0 text-capitalize text-primary" onDoubleClick={(e) => this.show(e)}>{translate("updateDate")}</th>
                                </tr>
                            </thead>
                            <tbody>
                                {this.state.tableData && this.state.tableData.map((rowData, index) => {
                                    return <tr key={index} onClick={(e) => this.selectRegion(rowData)}>
                                        <td>{translate(rowData.id)}</td>
                                        <td>{rowData.numsAll}</td>
                                        <td className="w-25">{rowData.date}</td>

                                    </tr>
                                })}


                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        );
    }

}

export default Overview;