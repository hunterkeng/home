import enData from '../lang/en-US.json';
import zhData from '../lang/zh-CN.json';

var trans;

export function setLocale(lang) {
    let locale = lang ? lang : navigator.language;
    trans = locale === "en-US" ? enData : zhData;
}

export function translate(key) {
    try {
        return trans[key] || key;
    } catch(err) {
        console.error(`--------translate translate: ${JSON.stringify(err)}--------------`)
    }
    
}