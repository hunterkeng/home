let events = [
    {
        "id": "20200301",
        "author": "x"
    },
    {
        "id": "20200302",
        "author": "m"
    },
    {
        "id": "20200303",
        "author": "q"
    },
    {
        "id": "20200304",
        "author": "z"
    }
]
console.log("--------Original--------");
console.log(events);

console.log("--------Add--------");
const add = { "id": "20200305", "author": "d" };
events.push(add);
console.log(events);

console.log("--------Delete--------");
let id = "20200303";
events = events.filter(x => x.id !== id);
console.log(events);

console.log("--------Update--------");
id = "20200304";
events = events.map(x => { if (x.id == id) { x.author = "aa" } return x; });
console.log(events);

console.log("--------Sort--------");
events = events.sort((x, y) => x.author > y.author ? 1 : -1);
console.log(events);


console.log("----------------");
console.log("----------------");

let d = new Date("2020-03-07");
d.setDate(d.getDate() + 7 - d.getDay() +1 )
let year = d.getFullYear();
let month = d.getMonth();
let dates = [];
for (var i = 0; i < 6; i++) {
    let da = new Date(year, month, (d.getDate() + 7 * i));
    dates.push(da)
}
console.log(dates)
console.log(new Date().toISOString());
console.log("----------------");

let a = [{"id":"2020-03-04T091214484Z","date":"Wed Mar 04 2020","subject":"Test66","from":"Test66","summary":"摘自1"},{"id":"2020-03-03T082912323Z","date":"Tue Mar 03 2020","subject":"Test5","from":"Test55","summary":"Test555"},{"id":"2020-03-03T082852061Z","date":"Tue Mar 03 2020","subject":"Test4","from":"Test4","summary":"摘自2"},{"id":"2020-03-03T064309811Z","date":"Tue Mar 03 2020","subject":"Test3","from":"Test33","summary":"Test333"},{"id":"2020-03-03T062218920Z","date":"Tue Mar 03 2020","subject":"Test2","from":"Test22","summary":"Test222"},{"id":"2020-03-04T091908542Z","date":"Wed Mar 04 2020","subject":"10","from":"response2","summary":"摘自3","lang":"en-US"}];
let b = JSON.stringify(a);
let c = encodeURIComponent(b);


let x = atob();
let y = decodeURIComponent(x);
let z = JSON.parse(y);


let reSymbol1 = 23;
let reSymbol2 = 24;

console.log(reSymbol1 + reSymbol2);

let dt = new Date();

dt.getUTCDate;
